package fr.upem.partiel.part2.functions

import fr.upem.partiel.part2.model.Movie
import fr.upem.partiel.part2.model.Movie.Director

object Functions {

  // TODO
  lazy val getDirectorNames: List[Movie] => List[String] = {
    case head :: tail => head.director.firstName + " " + head.director.lastName :: getDirectorNames(tail)
    case Nil => List("")

  }


  // TODO
  lazy val viewMoreThan: Long => List[Movie] => List[Movie] = i => lst => {lst.filter(_.views.number>i)
  }

  // TODO
  lazy val byDirector: List[Movie] => Map[Director, List[Movie]] = (lst:List[Movie]) => lst.groupBy(_.director)


}
