package fr.upem.partiel.part2.model

import fr.upem.partiel.part2.model.Movie._

// TODO You have to create all the classes you need for the exam
// TODO Don't forget to read the existing code and the unit tests to get some clues !

trait Movie{
  val title: Title
  val director: Director
  val year: Year
  val views: Views
  val country: Country
}

object Movie {

  def apply(title: Title, director: Director, year: Year, views: Views, country: Country): Movie = Movie(title, director, year, views, country)

  trait Title{ val name: String}


  trait Director{
    val firstName: String
    val lastName: String
  }


  trait Year{
    val value: Int
  }

  // TODO Create this model

  trait Views{ val number: Long}

  trait Country

  object Country {

    final case object France extends Country

    final case object England extends Country

    final case object Italy extends Country

    final case object Germany extends Country

    final case object UnitedStates extends Country

  }

  // TODO Create this method
  def movie(title: Title, director: Director, year: Year, views: Views, country: Country): Movie = Movie(title, director, year, views, country)

  // TODO Create this method
  def title(s: String): Title = TitleImpl(s)

  // TODO Create this method
  def director(first: String, last: String): Director = DirectorImpl(first, last)

  // TODO Create this method
  def year(value: Int): Year = YearImpl(value)

  // TODO Create this method
  def views(value: Long): Views = ViewsImpl(value)

  case class TitleImpl(name: String) extends Title

  case class YearImpl(value: Int) extends Year

  case class DirectorImpl(firstName: String, lastName: String) extends Director

  case class ViewsImpl(number: Long) extends Views

}